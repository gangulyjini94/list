import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import PostList from './PostList'
import Table from "./Table";
import Form from "./Form";
import LifecycleA from "./LifecycleA";
import PostListOne from "./PostListOne";


function App() {
  return (
    <div className="App">
     {/*<PostList/>*/}
     {/*/!*<Table/>*!/*/}
     {/*/!*<Form/>*!/*/}
     {/*/!*<LifecycleA/>*!/*/}
     <PostListOne/>
    </div>
  );
}

export default App;
