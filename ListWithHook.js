import React, {useState, useEffect} from "react";
import {Table} from 'react-bootstrap';
import axios from 'axios';
import AddUserForm from "./FormWithCreateRef";

function UserList() {
    const [posts, setPosts] = useState([]);
    useEffect(async () => {
        let ignore = false;
      /*  async function fetchData() {
            const {data} = await axios.get('https://jsonplaceholder.typicode.com/posts/100/comments');
            if (!ignore) setPosts(data);
        }
        fetchData();
*/
        const url = `http://3.0.94.193:4000/user`
        const users = await getUsers(url);
        //console.log(users);
        setPosts(users.data);
        return () => {ignore = true}
    }, []);

    const [showEditPopup, setShowEditPopup] = useState(false);
    const [editingPost, setEditingPost] = useState({});
    const getUsers = async (url) => {
        try {
            const token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDgzNTA5Yi0yYjliLTRmMDUtYTFlYS02ZWRmNmEwZmNmNGEiLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE1ODA4OTQxMDcsImV4cCI6MTU4MTc1ODEwN30.V_i0z4wNN-gOGI5MmqtzFwq0ObsLi__F8gANTeLOKCFK7x_yNlRSEroiDGUmosuDpz3eh0VFbLFC1WahgokUMw';
            if (token){
                const response = await fetch(url, {
                    method: 'GET',
                    credentials: 'include',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    },
                });
                // TODO implement check status
                //const { status } = response;
                const responseJson = await response.json();
                return Promise.resolve(responseJson);
            }
        } catch (ex) {
            console.log(ex);
        }
        return Promise.reject(new Error('get async is exception'))
    }
    const togglePopup = () => {
        setShowEditPopup(!showEditPopup)
    };
    const editHandle = (id) => {
        setShowEditPopup(true);
        setEditingPost(posts.find(post => post.id === id))
    };
    const deleteHandle = (id) => {
        setPosts(posts.filter(post => post.id !== id))
    };

    const handlerSave = (data) => {
        axios.post('http://3.0.94.193:4000/user', data)
            .then(response => {
                console.log(response);
                console.log('post successfully')
            }).catch( error => {
            console.log(error)
        })
    };
    console.log(posts)
    return (
        <div className="container">
            <h2>USER MANAGEMENT</h2>
            <button className="btn btn-dark" onClick={togglePopup}>Add New Employee</button>
            {showEditPopup ? <AddUserForm handlerFormSave={handlerSave} data={editingPost}/>:null}
            <Table striped bordered hover>
                <tr>
                    <th/>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Organisation</th>
                    <th>CreatedDate</th>
                    <th>Actions</th>
                </tr>
                {
                    posts.length ?
                        posts.map((post, index) =>
                            <tr key = {index}>
                                <td><input className="custom-checkbox" value={post.id} type="checkbox"/></td>
                                <td>{post.name}</td>
                                <td>{post.email}</td>
                                <td>{post.organisation}</td>
                                <td>{post.createdAt}</td>
                                <td width= "15%">
                                    <button className="btn btn-primary" onClick={() => editHandle(post.id)}>Edit</button>
                                    <button className="btn btn-danger" onClick={() => deleteHandle(post.id)}>Delete</button>
                                </td>
                            </tr>
                        ): null
                }
            </Table>
        </div>
    )
}

export default UserList